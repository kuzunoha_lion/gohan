import pytest
import sqlite3

from keisan import create_app, controller


@pytest.fixture(scope='session', autouse=True)
def setup():
    # データベースのテーブル作成

    with sqlite3.connect('test_tabetekitakiroku.db') as conn:
        c = conn.cursor()
        create_table = 'CREATE TABLE nanitabeta (id integer primary key AUTOINCREMENT,price int(128),datetime varchar(64))'
        c.execute(create_table)

    yield

    # データベースのテーブル削除

    with sqlite3.connect('test_tabetekitakiroku.db') as conn:
        c = conn.cursor()
        drop_table = 'DROP TABLE nanitabeta'
        c.execute(drop_table)


@pytest.fixture(scope='module', autouse=True)
def test_app():
    app = create_app('test')
    app.register_blueprint(controller.bluep)
    return app


@pytest.fixture(scope='module', autouse=True)
def client(test_app):
    return test_app.test_client()
