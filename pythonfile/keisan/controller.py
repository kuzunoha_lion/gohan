from flask import Blueprint, render_template, redirect, url_for, current_app, request, session, abort

from datetime import datetime
import sqlite3

from keisan import helper

bluep = Blueprint('bluep', __name__)


@bluep.route('/')
def first():
    dict_var = {}
    dbname = current_app.config['DATABASE']
    select = ''' SELECT price, datetime FROM nanitabeta WHERE  datetime LIKE ? '''
    now_month = datetime.now().strftime('%Y%m')
    now_month += '_%'
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    c.execute(select, (now_month,))
    test = c.fetchall().copy()
    if test == None:
        dict_var['db_list_nanitabeta'] = [0, 'null!']
    else:
        dict_var['db_list_nanitabeta'] = test
    conn.close()
    dict_var['total'] = 0
    for price in test:
        dict_var['total'] += price[0]

    return render_template(
        'index.html',
        dict_var=dict_var
    )


@bluep.route('/login')
def login():
    if 'userid' in session:
        return redirect(url_for('bluep.all_of'))
    else:
        return render_template('login.html')


@bluep.route('/sessions', methods=['POST'])
def sessions():
    user_id = request.form['user_id']
    password = request.form['password']
    if helper.login_kakunin(user_id, password):
        session['userid'] = user_id
        return redirect(url_for('bluep.all_of'))
    else:
        return abort(400)


@bluep.route('/logout')
def logout():
    session.pop('userid', None)
    return redirect(url_for('bluep.first'))


@bluep.route('/post_data', methods=['POST'])
def post_data():
    if not request.form['price'].isdecimal():
        return '半角数字入れて'
    price = int(request.form['price'])
    dbname = current_app.config['DATABASE']
    now_time = datetime.now().strftime('%Y%m%d_%H%M')
    insert_into = ''' INSERT INTO  nanitabeta (price, datetime) VALUES(?, ?)'''

    with sqlite3.connect(dbname) as conn:
        c = conn.cursor()
        c.execute(insert_into, (price, now_time))

    return redirect(url_for('bluep.first'))


@bluep.route('/all_of')
def all_of():
    dict_var = {}
    dbname = current_app.config['DATABASE']
    select = ''' SELECT * FROM nanitabeta '''

    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    c.execute(select)
    test = c.fetchall().copy()
    if test == None:
        dict_var['db_list_all'] = (0, 0, 'null!')
    else:
        dict_var['db_list_all'] = test
    conn.close()

    return render_template(
        'all_of.html',
        dict_var=dict_var
    )


@bluep.route('/edit', methods=['POST'])
def edit():
    if not request.form['id'].isdecimal():
        return '半角数字入れて'
    dict_var = {}
    db_id = int(request.form['id'])
    dbname = current_app.config['DATABASE']
    select = ''' SELECT * FROM nanitabeta WHERE id = ? '''

    with sqlite3.connect(dbname) as conn:
        c = conn.cursor()
        dict_var['confirm_row'] = c.execute(select, (db_id,))
    return render_template(
        'edit.html',
        dict_var=dict_var
    )


@bluep.route('/del_data', methods=['POST'])
def del_data():
    del_id = request.form['del_id']
    dbname = current_app.config['DATABASE']
    delete = ''' DELETE FROM nanitabeta WHERE id = ? '''

    with sqlite3.connect(dbname) as conn:
        c = conn.cursor()
        c.execute(delete, (del_id,))
    return redirect(url_for('bluep.first'))


@bluep.route('/edit_data', methods=['POST'])
def edit_data():
    edit_id = request.form['edit_id']
    edit_price = request.form['edit_price']
    edit_datetime = request.form['edit_datetime']

    dbname = current_app.config['DATABASE']
    update = ''' UPDATE nanitabeta SET price = ?, datetime = ? WHERE id = ? '''

    with sqlite3.connect(dbname) as conn:
        c = conn.cursor()
        c.execute(update, (edit_price, edit_datetime, edit_id))
    return redirect(url_for('bluep.first'))
