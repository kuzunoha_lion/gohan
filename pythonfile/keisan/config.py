class BaseConfig(object):
    DEBUG = False
    TESTING = False
    DATABASE = 'tabetakiroku.db'
    SECRET_KEY = '\xd8\xc1\xaa\xca\xe1\xa2\xd7\xa7C\x85\x19\xc8\xc2\xc1$\x9c\xf7X\x95\xa3\xc3\x0e'


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False
    DATABASE = 'tabetakiroku.db'
    SECRET_KEY = 'secret'


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    DATABASE = 'test_tabetekitakiroku.db'
    SECRET_KEY = '\xd8\xc1\xaa\xca\xe1\xa2\xd7\xa7C\x85\x19\xc8\xc2\xc1$\x9c\xf7X\x95\xa3\xc3\x0e'
