from flask import Flask
from keisan.database import init_db

configures = {
    'develp': 'keisan.config.DevelopmentConfig',
    'test': 'keisan.config.TestingConfig'
}


def create_app(config_mode='test'):

    app = Flask(__name__)
    app.config.from_object(configures[config_mode])

    return app
