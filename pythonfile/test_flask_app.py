from flask import session
import pytest
import sqlite3


def test_routing(client):
    assert 200 == client.get('/').status_code
    assert 200 == client.get('/all_of').status_code
    assert 200 == client.get('/login').status_code
    assert 405 == client.get('/edit').status_code
    assert 405 == client.get('/del_data').status_code
    assert 405 == client.get('/edit_data').status_code
    assert 302 == client.post('/del_data', data={'del_id': '1'}).status_code
    assert 302 == client.post(
        '/edit_data', data={
            'edit_id': '1',
            'edit_price': '130',
            'edit_datetime': '20190101_0000'}).status_code
    assert 404 == client.get('/aaa').status_code


def test_session(client):
    with client as c:
        with c.session_transaction() as sess:
            sess['userid'] = 'value'
        assert 302 == c.get('/login').status_code
        c.get('logout')
        assert 'userid' is not session


def test_post_data(client):
    assert '半角数字入れて' == client.post(
        '/post_data', data={'price': 'あ'}).data.decode('utf-8')
    assert '半角数字入れて' == client.post(
        '/post_data', data={'price': '#'}).data.decode('utf-8')
    assert 200 == client.post(
        '/post_data', data={'price': '123'}, follow_redirects=True).status_code
    assert '123' in client.get('/').data.decode('utf-8')
    assert '9,877円' in client.get('/').data.decode('utf-8')
    assert 200 == client.post(
        '/post_data', data={'price': '456'}, follow_redirects=True).status_code
    assert '9,421円' in client.get('/').data.decode('utf-8')


def test_login(client):
    assert 302 == client.post('/sessions', data={
        'user_id': 'kuzunoha',
        'password': 'ee3jHyMH'
    }).status_code
    assert 400 == client.post('/sessions', data={
        'user_id': 'failed',
        'password': 'ee3jHyMH'
    }).status_code
    assert 400 == client.post('/sessions', data={
        'user_id': 'kuzunoha',
        'password': 'failed'
    }).status_code
