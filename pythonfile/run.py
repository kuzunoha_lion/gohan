from keisan import create_app, controller

app = create_app(config_mode='develp')

app.register_blueprint(controller.bluep)

#app.secret_key = app.config['SECRET']

if __name__ == '__main__':
    app.run()
