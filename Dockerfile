FROM python:3.6.6
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY pythonfile /var/pythonfile
WORKDIR /var/pythonfile
RUN find . -name \*.pyc -delete
CMD pytest && uwsgi --ini uwsgi.ini